# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "precise64"
  config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  #Omnibus Vagrant plugin
  config.omnibus.chef_version = :latest

  #Developing Code dir.
  src_host_dir = '../app'
  src_guest_dir = '/share'

  #Data archive dir.(ex, dump file, config file etc...)
  dump_host_dir = '../backup'
  dump_guest_dir = '/backup'

  #WebServer Document Root
  doc_root = '/share'

  # provider VB
  config.vm.provider :virtualbox do |vb, override|
    override.vm.network :private_network, ip: "192.168.33.33"
    override.vm.synced_folder src_host_dir, src_guest_dir, \
      :create => true, \
      :owner=> 'vagrant', \
      :group=>'www-data', \
      mount_options:['dmode=775,fmode=775']
    override.vm.synced_folder dump_host_dir, dump_guest_dir, \
      :create => true, \
      :owner => 'vagrant', \
      :group => 'vagrant', \
      mount_options:['dmode=775,fmode=755']
  end

  # provider AWS
  config.vm.provider :aws do |aws, override|
    aws.access_key_id     = ENV['AWS_ACCESS_KEY_ID']
    aws.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
    aws.keypair_name = ENV['AWS_KEYPAIR_NAME']

    override.vm.box = "dummy"
    override.vm.box_url = "https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box"
    override.vm.synced_folder src_host_dir, src_guest_dir, \
      :create => true, \
      :owner=> 'ubuntu', \
      :group=>'www-data', \
      mount_options:['dmode=775,fmode=775']
    override.vm.synced_folder dump_host_dir, dump_guest_dir, \
      :create => true, \
      :owner => 'ubuntu', \
      :group => 'ubuntu', \
      mount_options:['dmode=775,fmode=755']
    override.ssh.username = 'ubuntu'
    override.ssh.private_key_path = ENV['AWS_PRIVATE_KEY_PATH']

    aws.instance_type = "t1.micro"
    aws.region = "ap-northeast-1"
    aws.ami = "ami-8f78188e"
    aws.security_groups = [ 'default', 'lnmp', 'webserver' ]
  end

  # provisioning
  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = ["./cookbooks", "./site-cookbooks"]
    # chef.cookbooks_path = "./cookbooks"
    chef.add_recipe "omusubi"
    chef.add_recipe "database::mysql"
    chef.add_recipe "mysql"
    chef.json = {
      doc_root: doc_root,
      mysql: {
        server_root_username: "dev0ps",
        server_root_password: "dev0psPw"
      }
    }
  end
end
