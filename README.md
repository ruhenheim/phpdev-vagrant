# development for php on Vagrant & AWS

## My ruhenheim specific platform
- OS : ubuntu precise64
- Web: nginx/1.1.19
- DB : mysql/5.5
- PHP: php5-fpm/5.3.10

## Getting Started

####1. clone this repository

```
git clone https://bitbucket.org/ruhenheim/phpdev-vagrant.git
```

####2. boot on VirtualBox

```
cd vagrant
vagrant up
```

it take a few minutes... coffee break.

####3. boot on AWS

at first. check your AWS access info.
check AWS Environment variables in ***vagrant/Vagrantfile***
ex. `ENV['AWS_ACCESS_KEY_ID']`, `ENV['AWS_SECRET_ACCESS_KEY']`

at Second. check your EC2 SecurityGroup IDs.
We need SSH permission for this. check the Group IDs that can communicate.
check `aws.security_groups` in ***vagrant/Vagrantfile***

```
cd vagrant
vagrant up --provider=aws
```

it take a few minutes... coffee break.

## Requirement

#### vagrant plugin

```
~  vagrant plugin list
vagrant-aws (=>0.4.1)
vagrant-omnibus (=>1.2.1)
```